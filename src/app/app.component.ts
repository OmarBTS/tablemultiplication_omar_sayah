import { Component} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'multitable';

  // Déclaration de la variable (typée !) pour l'utilisateur
  app: string = '';  
  // Fonction retournant un booléen et non le nom
  getConnect() {
    return this.app != '';
  }
}