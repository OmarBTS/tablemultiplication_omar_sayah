import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MultiplicationComponent } from './multiplication/multiplication.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MultiplicationbisComponent } from './multiplicationbis/multiplicationbis.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MultiplicationComponent,
    MultiplicationbisComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
