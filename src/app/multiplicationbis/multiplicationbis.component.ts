import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-multiplicationbis',
  templateUrl: './multiplicationbis.component.html',
  styleUrls: ['./multiplicationbis.component.scss']
})
export class MultiplicationbisComponent implements OnInit {

  identForm!: FormGroup;
  isSubmitted2 = false;
  badLogin2 = false;
  chif2 = 0;
  tab2= [{num: 1},{num: 2},{num: 3},{num: 4},{num: 5},{num: 6},{num: 7},{num: 8},{num: 9},{num: 10}];
  nbTab = Array();

  constructor() { }

  ngOnInit(): void {
    this.identForm = new FormGroup({
      chif2: new FormControl()
    });

  }
  get formControls() { return this.identForm.controls; }
  mult2() {
    this.chif2 = this.identForm.get('chif2')?.value;
    console.log();
    this.isSubmitted2 = true;
    console.log("number : " + this.identForm.value);
    if (this.identForm.value.chif2 == '') {
      this.badLogin2 = true;
      return;
    } else {
      this.badLogin2 = false;
      this.nbTab=[];
      const number = this.identForm.value.chif2;
      for (let j = 1; j <= number; j++) {
        this.nbTab.push(j);
        console.log(this.nbTab);

        for (let i = 1; i <= 10; i++) {
          const result = i * j;
          console.log(`${j} * ${i} = ${result}`);
          console.log(j);

        }
      }
    }
  }
}
