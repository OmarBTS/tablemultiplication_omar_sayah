import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiplicationbisComponent } from './multiplicationbis.component';

describe('MultiplicationbisComponent', () => {
  let component: MultiplicationbisComponent;
  let fixture: ComponentFixture<MultiplicationbisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MultiplicationbisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiplicationbisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
