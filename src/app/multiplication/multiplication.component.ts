import { Component, Output, OnInit, EventEmitter} from '@angular/core';
import { FormGroup, FormControl} from '@angular/forms';

@Component({
  selector: 'app-multiplication',
  templateUrl: './multiplication.component.html',
  styleUrls: ['./multiplication.component.scss']
})
export class MultiplicationComponent implements OnInit {

  identForm!: FormGroup;
  isSubmitted = false;
  badLogin = false;
  tab1 = [{num: 1},{num: 2},{num: 3},{num: 4},{num: 5},{num: 6},{num: 7},{num: 8},{num: 9},{num: 10}];
  chif1 = 0;

  @Output()leTableau = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    this.identForm = new FormGroup({
      chif1: new FormControl('')
    });
  }
  get formControls(){return this.identForm.controls;}
  mult() {
    this.chif1=this.identForm.get('chif1')?.value;
    this.isSubmitted = true;
    console.log("number : " + this.identForm.value);
    if(this.identForm.value.chif1==''){
      this.badLogin = true;
      return;
    }else{
      this.badLogin = false;
      const number = this.identForm.value.chif1;
      for(let i=1; i<=10; i++){
        const result = i*number;
        console.log(`${number} * ${i} = ${result}`);
      }
      this.leTableau.emit(this.identForm.value.chif1);
    }
  }

}